const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const request = require('request');
const fs = require('fs');
const child = require('child_process');
const os = require('os');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('static'));

app.get('/ping', function (req, res) {
    get_sys_info('high nigga');
    res.send('pong');
});

app.post('/query', function (req, res) {
    let json = req.body;
    console.log('req body:', json);
    let uri = 'http://192.168.1.152:5000'+json.path;
    request({
        url: uri,
        method: json.method,
        json: json.body?json.body:{}
    }, function (err, resp, body) {
        if (err) console.log(err);
        res.send(handle_struct(body));
        get_sys_info(json.term);
    })

})

app.post('/baidu', function (req, res) {
    let json = req.body;
    console.log('req body:', json);
    let uri = 'http://192.168.1.152:5000'+json.path;
    request({
        url: uri,
        method: json.method,
        json: json.body?json.body:{}
    }, function (err, resp, body) {
        if (err) console.log(err);
        res.send(baidu_format(body));
    })
})

let server = app.listen(3000, function () {
    let host = server.address().address;
    let port = server.address().port;

    console.log('app listening at http://%s:%s', host, port);
});

function handle_struct(json) {
    let raw_json = {
        results: [
            {
                columns: [],
                data: [{
                    graph: {
                        nodes: [],
                        relationships: []
                    }
                }]
            }
        ],
        errors: []
    };
    json.data.graph.entities.forEach(function (item) {
        item.labels = ['User'];
        raw_json.results[0].data[0].graph.nodes.push(item);
    });
    json.data.graph.relationships.forEach(function (item) {
        let elem = {
            id: item.id,
            type: item.type,
            startNode: item.source,
            endNode: item.target,
            properties: item.properties
        };
        raw_json.results[0].data[0].graph.relationships.push(elem);
    })
    return raw_json;
}

function baidu_format(json) {
    let raw = {
        nodes: [],
        links: []
    };
    json.data.graph.entities.forEach(function (item) {
        let node = {
            id: item.id,
            attributes: {
                modularity_class: 0
            },
            category: 0,
            draggable: true,
            name: item.name,
            itemStyle: null,
            symbolSize: 10,
            value: 10,
            x: null,
            y: null
        };
        raw.nodes.push(node);
    });
    json.data.graph.relationships.forEach(function (item) {
        let link = {
            id: item.id,
            lineStyle: {
                normal: {}
            },
            name: item.type,
            source: item.source,
            target: item.target
        };
        raw.links.push(link);
    });
    console.log('baidu format',raw);
    return raw;
}

function get_sys_info(term) {
    child.exec('python ../py_sys/info.py '+term, function (err, stdout, stderr) {
        console.log(err || stdout);
    })
}